# CS371p: Object-Oriented Programming Collatz Repo

* Name: Simon Pinochet Concha

* EID: sp44454

* GitLab ID: simon.pinochet

* HackerRank ID: simon_pinochet

* Git SHA: aca963da72d71216ff7f56be438ceb49be8408ff

* GitLab Pipelines: https://gitlab.com/simon.pinochet/cs371p-collatz/pipelines

* Estimated completion time: 2 hours

* Actual completion time: About 7 hours

* Comments: I'm taking CS373 alongside this course so I finished that first, reused the code for this project and translated it to C++.
